Vehico Translations
-------------------

Translations files are in JSON format named as `messages.{cs,pl,en,...}.json`
and can by downloaded as a [zip archive](http://translations.vehico.online/locales.zip).


OneSky
------
http://vehico.oneskyapp.com/admin/project/dashboard/project/30088
