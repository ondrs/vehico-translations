// Generated on 2013-10-15 using generator-angular 0.4.0
'use strict';


// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-json-minify');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-merge-json');

  grunt.initConfig({

    'merge-json': {
      'i18n': {
        files: {
          'www/locales/messages.en.json': ['src/**/messages.en.json'],
          'www/locales/messages.cs.json': ['src/**/messages.cs.json'],
          'www/locales/messages.pl.json': ['src/**/messages.pl.json']
        }
      }
    },

    'json-minify': {
      build: {
        files: 'www/**/*.json'
      }
    },

    'compress': {
      zip: {
        options: {
          archive: 'www/locales.zip',
          mode: 'zip'
        },
        files: [
          {expand: true, cwd: 'www/locales/', src: ['**']}
        ]
      }
    }

  });

  grunt.registerTask('default', [
    'merge-json',
    'json-minify',
    'compress'
  ]);

};
